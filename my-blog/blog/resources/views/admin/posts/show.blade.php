@extends('admin.layout.layout')

@section('page_title','Show Posts');

@section('container')
         
<div class="page-title">
  <div class="title_left ml-2">
    <h4 class="ml-2 mt-2">All Posts</h4>
  </div>
</div>
<div class="row">
	@if(session('success'))
	      <div class="success-color">{{session('success')}}</div>
    @elseif(session('failed'))
	       <div class="error-color">{{session('failed')}}</div>
    @endif		   		  
</div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
     <div class="row">
      <div class="col-sm-12">
       <div class="card-box table-responsive">
        <table class="table table-bordered">
				   <thead>
					   <tr class="bg-dark text-white">
						    <th>Sr.No.</th>
							  <th>Title</th>
							  <th>Short Desc</th>
							  <th>Long Desc</th>
							  <th>Blog Date</th>
							  <th>Action</th>
					   </tr>
				  </thead>
          <tbody>
          	@foreach($post as $key)
			  @if($key->status==1)
	          <tr>
							  <td>{{$key->id}}</td>
							  <td>{{$key->title}}</td>
							  <td>{{$key->short_desc}}</td>
							  <td>{{$key->long_desc}}</td>
							  <td>{{$key->blog_date}}</td>
							  <td>
							  	<a href="{{route('post.edit',$key->id)}}"><em class="fa fa-edit mr-4" style="font-size:30px;"></em></a>
							  	<a href="{{route('post.delete',$key->id)}}" onclick="return confirm('Are You Sure !'); "><em class="fa fa-trash" style="color:red;font-size:30px;"></em></a>
							  </td>
            </tr>
			  @endif
            @endforeach

          </tbody>  
        </table>
      </div>
     </div>
    </div>
  </div>
</div>

@endsection
