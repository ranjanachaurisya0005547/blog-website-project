@extends('admin.layout.layout')

@section('page_title','Add Pages')

@section('container')
<div class="page-title">
      <div class="title_left ml-2">
        <h4 class="ml-2 mt-2">Add Pages</h4>
      </div>
</div>

@if(session('success'))
   <span class="success-color">{{session('success')}}</span>
@elseif(session('failed'))
   <span class="error-color">{{session('failed')}}</span>

@endif

<div class="col-md-12 col-sm-12"><div class="x_panel">
<div class="x_content">
	<br />
	<form action="{{route('page.update')}}" method="POST" id="demo-form2" enctype="multipart/form-data">

    @csrf
	<input type="hidden" name="id" value="{{$allPage->id}}"/>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="name"> 
			   Name <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 ">
				<input type="text" id="name" name="name" value="{{$allPage->name}}" class="form-control ">
				@error('name')
           <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="slug">Slug<span class="required">*</span>
		   </label>
			<div class="col-md-6 col-sm-6 ">
			   <input type="text" name="slug" id="slug" class="form-control" value="{{$allPage->slug}}"/>
			   @error('slug')
                  <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="desc">Description <span class="required">*</span>
		   </label>
			<div class="col-md-6 col-sm-6 ">
			   <textarea name="description" id="desc" class="form-control" cols="10" rows="12">
			   	   {{$allPage->description}}
			   </textarea>
			   @error('description')
                   <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<div class="col-md-6 col-sm-6 offset-md-3">
				<input type="submit" class="btn btn-primary" name="submit-btn" value="Submit"/>
			</div>
		</div>
	</form>


</div>
</div>
</div>
	
@endsection


