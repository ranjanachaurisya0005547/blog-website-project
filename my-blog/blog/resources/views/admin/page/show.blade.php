@extends('admin.layout.layout')

@section('page_title','Show Pages');

@section('container')
         
<div class="page-title">
  <div class="title_left ml-2">
    <h4 class="ml-2 mt-2">All Pages</h4>
  </div>
</div>
<div class="row">
	@if(session('success'))
	      <div class="success-color">{{session('success')}}</div>
    @elseif(session('failed'))
	       <div class="error-color">{{session('failed')}}</div>
    @endif		   		  
</div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
     <div class="row">
      <div class="col-sm-12">
       <div class="card-box table-responsive">
        <table class="table table-bordered">
				   <thead>
					   <tr class="bg-dark text-white">
						    <th>Sr.No.</th>
							  <th>Name</th>
							  <th>Slug</th>
							  <th>Created</th>
							  <th>Action</th>
					   </tr>
				  </thead>
          <tbody>
          	@foreach($page as $key)
	          <tr>
							  <td>{{$key->id}}</td>
							  <td>{{$key->name}}</td>
							  <td>{{$key->slug}}</td>
							  <td>{{$key->created_at}}</td>
							  <td>
							  	<a href="{{route('page.edit',$key->id)}}"><em class="fa fa-edit mr-4" style="font-size:30px;"></em></a>
							  	<a href="{{route('page.delete',$key->id)}}" onclick="return confirm('Are You Sure !'); "><em class="fa fa-trash" style="color:red;font-size:30px;"></em></a>
							  </td>
            </tr>
            @endforeach

          </tbody>  
        </table>
      </div>
     </div>
    </div>
  </div>
</div>

@endsection