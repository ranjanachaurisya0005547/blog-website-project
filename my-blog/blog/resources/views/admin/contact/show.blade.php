@extends('admin.layout.layout')

@section('page_title','Show Contacts');

@section('container')
         
<div class="page-title">
  <div class="title_left ml-2">
    <h4 class="ml-2 mt-2">All Contacts</h4>
  </div>
</div>
<div class="row">
	@if(session('success'))
	      <div class="success-color">{{session('success')}}</div>
    @elseif(session('failed'))
	       <div class="error-color">{{session('failed')}}</div>
    @endif		   		  
</div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
     <div class="row">
      <div class="col-sm-12">
       <div class="card-box table-responsive">
        <table class="table table-bordered">
				   <thead>
					   <tr class="bg-dark text-white">
						    <th>Sr.No.</th>
							  <th>Name</th>
							  <th>Email</th>
							  <th>Mobile</th>
							  <th>Message</th>
					   </tr>
				  </thead>
          <tbody>
          	@foreach($contact as $key)
	          <tr>
							  <td>{{$key->id}}</td>
							  <td>{{$key->name}}</td>
							  <td>{{$key->email}}</td>
							  <td>{{$key->mobile}}</td>
							  <td>{{$key->message}}</td>
            </tr>
            @endforeach

          </tbody>  
        </table>
      </div>
     </div>
    </div>
  </div>
</div>

@endsection