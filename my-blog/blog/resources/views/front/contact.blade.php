@extends('front.layout.layout')

@section('page_title','Contact Us.....')

@section('container')

<header class="masthead" style="background-image: url('{{asset('front_assets/assets/img/contact-bg.jpg')}}">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="page-heading">
                            <h1>Contact Me</h1>
                            <span class="subheading">Have questions? I have answers.</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Content-->
        <main class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <p>Want to get in touch? Fill out the form below to send me a message and I will get back to you as soon as possible!</p>
                        <div class="my-5">
                            @if(session('success'))
                               <div class="success-color">{{session('success')}}</div>
                            @elseif(session('failed'))
                                <div class="error-color">{{session('failed')}}</div>
                            @endif
                            <form action="{{route('blog.contact')}}" method="POST">

                            @csrf
                                <div class="form-floating">
                                    <input class="form-control" name="name" id="name" type="text" placeholder="Enter your name..." data-sb-validations="required" />
                                    <label for="name">Name</label>
                                    @error('name')
                                    <div class="error-color">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-floating">
                                    <input class="form-control" name="email" id="email" type="email" placeholder="Enter your email..." data-sb-validations="required,email" />
                                    <label for="email">Email address</label>
                                    @error('email')
                                       <div class="error-color">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-floating">
                                    <input class="form-control" name="mobile" id="phone" type="text" placeholder="Enter your phone number..." data-sb-validations="required" />
                                    <label for="phone">Phone Number</label>
                                    @error('mobile')
                                       <div class="error-color">A phone number is required.</div>
                                    @enderror
                                    </div>
                                <div class="form-floating">
                                    <textarea class="form-control" name="message" id="message" placeholder="Enter your message here..." style="height: 12rem" data-sb-validations="required"></textarea>
                                    <label for="message">Message</label>
                                    @error('message')
                                       <div class="error-color">{{$message}}</div>
                                    @enderror
                                </div>
                                <br />
                        
                                <input class="btn btn-primary text-uppercase " type="submit" value="SEND">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection