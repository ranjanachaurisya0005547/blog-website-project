@extends('front.layout.layout')

@section('page_title','My Posts.....')

@section('container')

<header class="masthead" style="background-image: url('{{asset('front_assets/assets/img/post-bg.jpg')}}">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1>Man must explore, and this is exploration at its greatest</h1>
                            <h2 class="subheading">Problems look mighty small from 150 miles up</h2>
                            <span class="meta">
                                Posted on August 24, 2021
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Post Content-->
        <article class="mb-4">
        @foreach($allPost as $list)
<div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="{{route('blog.singlepost',$list->id)}}">
                            <h2 class="post-title">{{$list->title}}</h2>
                            <h3 class="post-subtitle">{{$list->short_desc}}</h3>
                        </a>
                        <p class="post-meta">
                            Posted on {{$list->blog_date}}
                        </p>
                        <div class="d-flex justify-content mb-4"><a class="btn btn-primary text-uppercase" href="{{route('blog.singlepost',$list->id)}}">Read More</a></div>
                </div>
        </div>
        <!-- Divider-->
        <hr class="my-4" />
     </div>
 </div>
@endforeach
        </article>

@endsection