@extends('front.layout.layout')

@section('page_title',$singlePost[0]->title)

@section('container')

<header class="masthead" style="background-image: url('{{asset('blog_image/'.$singlePost[0]->image)}}')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="site-heading">
                            <h1>{{$singlePost[0]->title}}</h1>
                            <span class="subheading">{{$singlePost[0]->short_desc}}</span>
                        </div>
                    </div>
                </div>
            </div>
</header>
<!-- Post Content-->
<article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <!--------title-------->
                        <h3>{{$singlePost[0]->title}}</h3>
                        <!---------short Description----------->
                        <b>{{$singlePost[0]->short_desc}}</b>
                        <!---------------Long Description----------------->
                        <p>{{$singlePost[0]->long_desc}}</p>
                    </div>
                </div>
            </div>
        </article>


@endsection