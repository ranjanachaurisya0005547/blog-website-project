<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    public function home(){
        $post=Post::all();
        return view('front.home',['allPost'=>$post]);
    }

    //about view
    public function about(){
        return view('front.about');
    }

    //post view
    public function post(){
        $post=Post::all();
        return view('front.post',['allPost'=>$post]);
    }


    //contact view
    public function contact(){
        return view('front.contact');
    }

    //Blog Single Post
    public static function singlePost($id){
        $singlePost=Post::where('id',$id)->get();
        return view('front.post-details',compact('singlePost'));
    }

   
}
