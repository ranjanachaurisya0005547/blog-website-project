<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use Validator;

class ContactController extends Controller
{
    //Show Contact Users 
    public function showContact(){
        try{
            $contact=Contact::all();
            return view('admin.contact.show',['contact'=>$contact]);

        }catch(Exception $ex){
           return redirect(route('post.show'))->with('failed',$ex->getMessage());
        }
    }


    //Store Contact Users
    public function storeContact(Request $request){
        $contact=$request->input();
        
        $rules=[
            'name'=>'required',
            'email'=>'required|email',
            'mobile'=>'required|max:10|regex:/^[6-9]{1}[0-9]{9}$/',
            'message'=>'required',
        ];

        $validate=Validator::make($request->all(),$rules);

        if($validate->fails()){
            return back()->withErrors($validate);
        }else{
            try{
                $userContact=Contact::create([
                    'name'=>$contact['name'],
                    'email'=>$contact['email'],
                    'mobile'=>$contact['mobile'],
                    'message'=>$contact['message'],
                ]);
                $status=$userContact->save();

                if($status){
                    return back()->with('success','Message Send Successfully !');
                }else{
                    return back()->with('failed','Message Not Send !');
                }

            }catch(\Exception $ex){
                return back()->with('failed',$ex->getMessage());
            }
        }
    }

}
