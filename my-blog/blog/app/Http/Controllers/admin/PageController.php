<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use Validator;

class PageController extends Controller
{
   
   //create blog
    public function storePages(Request $request){

      $rules=[
             'name'=>'required|regex:/^[A-Za-z ]+$/',
             'slug'=>'required|regex:/^[a-z]{1,}[-]{1}[A-Za-z]{1,}$/',
             'description'=>'required',
       ];    
      $validator=Validator::make($request->all(),$rules);

      if($validator->fails()){
          return redirect(route('page.add'))->withErrors($validator)->withInput();
      }else{
          try{
                   $data=$request->input();
              
                   $page=Page::create([
                      'name'=>$data['name'],
                      'slug'=>$data['slug'],
                      'description'=>$data['description'],
                   ]);
                   $status=$page->save();

                   if($status){
                        return redirect(route('page.add'))->with('success','Page Added Successfully !');
                   }else{
                        return redirect(route('page.add'))->with('failed','Page Not Added !');
                   }
          }catch(Exception $ex){
              return redirect(route('page.add'))->with('failed',$ex->getMessage());
          }
    }
  }  
    

    //Show Blog Posts
    public function showPages(){
        try{
             $page=Page::all();
             return view('admin.page.show',['page'=>$page]);

        }catch(Exception $ex){
            return redirect(route('page.add'))->with('failed',$ex->getMessage());
        }
      }


    //post edit view
      public function editView($id){
           $allPage=Page::find($id);
           return view('admin.page.edit',compact('allPage'));
      }

      
      //Post Update
      public function updatePage(Request $request){
          $rules=[
             'name'=>'required|regex:/^[A-Za-z ]+$/',
             'slug'=>'required|regex:/^[a-z]{1,}[-]{1}[a-z]{1,}$/',
             'description'=>'required',
       ];    
        $validator=Validator::make($request->all(),$rules);
  
        if($validator->fails()){
            return redirect(route('page.add'))->withErrors($validator)->withInput();
        }else{
           try{
                
                $page=$request->input();
                $status=Page::where('id',$page['id'])->update([
                    'name'=>$page['name'],
                    'slug'=>$page['slug'],
                    'description'=>$page['description'],
               ]);
               
               if($status){
                    return redirect(route('page.show'))->with('success','Page Updated Successfully !');
               }else{
                    return redirect(route('page.show'))->with('failed','Page Not Updated !');
               }

           }catch(\Exception $ex){
                return back()->with('failed',$ex->getMessage());
           }
          }

      }

      //delete post
      public function destroyPage($id){  
          try{
                 $page=Page::find($id);
				 $status=$page->delete();
                 
                 if($status){
                      return redirect(route('page.show'))->with('success','Page Deleted Successfully !');
                 }else{
                      return redirect(route('Page.show'))->with('failed','Page Not Deleted !');
                 }

          }catch(\Exception $ex){
               return redirect(route('page.add'))->with('failed',$ex->getMessage());
          }
                 
      }


       //View All Pages
    public static function pageView(){
     $pages=Page::all();
     return $pages;
 }

}
