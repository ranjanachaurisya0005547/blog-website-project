<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Validator;

class PostController extends Controller
{
   
   //create blog
    public function storePosts(Request $request){

      $rules=[
             'title'=>'required|regex:/^[A-Za-z ]+$/',
             'short_desc'=>'required|regex:/^[A-Za-z ]+$/',
             'long_desc'=>'required|regex:/^[A-Za-z ]+$/',
             'image'=>'required|mimes:jpeg,jpg,png',
             'blog_date'=>'required',
       ];    
      $validator=Validator::make($request->all(),$rules);

      if($validator->fails()){
          return redirect(route('post.add'))->withErrors($validator)->withInput();
      }else{
          try{
                   $data=$request->input();
                   $file_obj=$request->file('image');
                   $file_name=$file_obj->getClientOriginalName();
                   $file_obj->move(public_path('blog_image'),$file_name);

                   $post=Post::create([
                      'title'=>$data['title'],
                      'short_desc'=>$data['short_desc'],
                      'long_desc'=>$data['long_desc'],
                      'image'=>$file_name,
                      'blog_date'=>$data['blog_date'],
                   ]);
                   $status=$post->save();

                   if($status){
                        return redirect(route('post.add'))->with('success','Post Added Successfully !');
                   }else{
                        return redirect(route('post.add'))->with('failed','Post Not Added !');
                   }
          }catch(Exception $ex){
              return redirect(route('post.add'))->with('failed',$ex->getMessage());
          }
    }
  }  
    

    //Show Blog Posts
    public function showPosts(){
        try{

             $post=Post::all();

              return view('admin.posts.show',['post'=>$post]);

        }catch(Exception $ex){
            return redirect(route('post.add'))->with('failed',$ex->getMessage());
        }
      }


    //post edit view
      public function editView($id){
           $allPost=Post::find($id);
           return view('admin.posts.edit',compact('allPost'));
      }

      
      //Post Update
      public function updatePost(Request $request){
          $rules=[
               'title'=>'required|regex:/^[A-Za-z ]+$/',
               'short_desc'=>'required|regex:/^[A-Za-z ]+$/',
               'long_desc'=>'required|regex:/^[A-Za-z ]+$/',
               'image'=>'mimes:jpeg,jpg,png',
         ];    
        $validator=Validator::make($request->all(),$rules);
  
        if($validator->fails()){
            return redirect(route('post.add'))->withErrors($validator)->withInput();
        }else{
           try{
                if($request->hasFile('image')){
                     $file=$request->file('image');
                     $fileName=$file->getClientOriginalName();
                     $request->move(public_path('/blog_image'),$fileName);

                      $postImage=new Post();
                      $postImage->image=$fileName;
                }
                $post=$request->input();
                $status=Post::where('id',$post['id'])->update([
                    'title'=>$post['title'],
                    'short_desc'=>$post['short_desc'],
                    'long_desc'=>$post['long_desc'],
                    'blog_date'=>$post['blog_date'],
               ]);
               
               if($status){
                    return redirect(route('post.show'))->with('success','Post Updated Successfully !');
               }else{
                    return redirect(route('post.show'))->with('failed','Post Not Updated !');
               }

           }catch(\Exception $ex){
                return back()->with('failed',$ex->getMessage());
           }
          }

      }

      //delete post
      public function destroyPost($id){
           
          try{
                 $post=Post::find($id);
                 $status=$post->delete();
                 if($status){
                      return redirect(route('post.show'))->with('success','Post Deleted Successfully !');
                 }else{
                      return redirect(route('post.show'))->with('failed','Post Not Deleted !');
                 }

          }catch(\Exception $ex){
               return redirect(route('post.add'))->with('failed',$ex->getMessage());
          }
                 
      }

}
