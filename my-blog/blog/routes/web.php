<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\admin\PostController;
use App\Http\Controllers\admin\PageController;
use App\Http\Controllers\admin\ContactController;
use App\Http\Controllers\HomeController;


Route::get('/',[HomeController::class,'home'])->name('home');
Route::prefix('blog')->group(function(){
    Route::get('/about-us',[HomeController::class,'about'])->name('about');
    Route::get('/all-post',[HomeController::class,'post'])->name('post');
    Route::get('/contact-page',[HomeController::class,'contact'])->name('contact');
    Route::post('/contact',[ContactController::class,'storeContact'])->name('blog.contact');

    Route::get('/post/{id}',[HomeController::class,'singlePost'])->name('blog.singlepost');
});


Route::get('/admin/login',[LoginController::class,'loginView']);
Route::post('/login_submit',[LoginController::class,'login'])->name('login');

Route::prefix('admin/post')->group(function(){
    Route::middleware(['admin_auth'])->group(function(){
         Route::get('/show',[PostController::class,'showPosts'])->name('post.show');
         Route::view('/view','admin.posts.add')->name('post.add');
         Route::post('/add',[PostController::class,'storePosts'])->name('post.send');
         Route::get('/edit/{id}',[PostController::class,'editView'])->name('post.edit'); 
         Route::post('/update',[PostController::class,'updatePost'])->name('post.update');
         Route::get('/delete/{id}',[PostController::class,'destroyPost'])->name('post.delete');
         Route::get('/contact',[ContactController::class,'showContact'])->name('user.details');
         Route::get('/logout',[LoginController::class,'logout'])->name('logout'); 
    });
});

Route::prefix('admin/page')->group(function(){
    Route::middleware(['admin_auth'])->group(function(){
         Route::get('/show',[PageController::class,'showPages'])->name('page.show');
         Route::view('/view','admin.page.add')->name('page.add');
         Route::post('/add',[PageController::class,'storePages'])->name('page.send');
         Route::get('/edit/{id}',[PageController::class,'editView'])->name('page.edit'); 
         Route::post('/update',[PageController::class,'updatePage'])->name('page.update');
         Route::get('/delete/{id}',[PageController::class,'destroyPage'])->name('page.delete');
    });
});


